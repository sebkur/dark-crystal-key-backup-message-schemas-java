package org.magmacollective.darkCrystalKeyBackupSchemas;

import com.google.protobuf.InvalidProtocolBufferException;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.magmacollective.darkCrystalKeyBackupMessageSchemas.RootV1.Root;

class TestRoot {
    @Test void testRoot() {
        final long timeStamp = System.currentTimeMillis();
        Root root = Root.newBuilder()
                .setVersion("1.0.0")
                .setType("dark-crystal/root")
                .setTimestamp(timeStamp)
                .setQuorum(3)
                .setShards(5)
                .setName("directions to treasure")
                .setTool("dsprenkles/sss")
                .build();
        final byte[] rootByteArray = root.toByteArray();
        try {
            Root rootParsed = Root.parseFrom(rootByteArray);
            assertTrue(rootParsed.isInitialized(), "All required fields set");
            assertTrue(rootParsed.getVersion().equals("1.0.0"), "Correct version");
            assertTrue(rootParsed.getType().equals("dark-crystal/root"), "Correct type");
            assertTrue(rootParsed.getTimestamp() == timeStamp, "Correct timestamp");
            assertTrue(rootParsed.getQuorum() == 3, "Correct quorum");
            assertTrue(rootParsed.getShards() == 5, "Correct number of shards");
            assertTrue(rootParsed.getName().equals("directions to treasure"), "Correct name");
            assertTrue(rootParsed.getTool().equals("dsprenkles/sss"), "Correct tool");
        }  catch (InvalidProtocolBufferException e) {
            System.out.println("Error"); //TODO assert no error
        }
     }
}
