package org.magmacollective.darkCrystalKeyBackupMessageSchemas;

import com.google.protobuf.ByteString;

public class Publish {
  public final String VERSION = "1.0.0";
  public final String TOOL = "dsprenkles/sss";

  public byte[] buildRoot(int quorum, int shards, String name) {
    // TODO decide how to build root id eg: hash this buffer / add a random tag and hash everything
    RootV1.Root root = RootV1.Root.newBuilder()
            .setVersion(VERSION)
            .setType("dark-crystal/root")
            .setTimestamp(System.currentTimeMillis())
            .setQuorum(quorum)
            .setShards(shards)
            .setName(name)
            .setTool(TOOL)
            .build();
    return root.toByteArray();
  }

  public byte[] buildShard(byte[] rootId, byte[] recipient, byte[] shardData) {
    return ShardV1.Shard.newBuilder()
            .setVersion(VERSION)
            .setType("dark-crystal/shard")
            .setTimestamp(System.currentTimeMillis())
            .setRecipient(ByteString.copyFrom(recipient))
            .setShard(ByteString.copyFrom(shardData))
            .setRoot(ByteString.copyFrom(rootId))
            .build()
            .toByteArray();
  }

  public byte[] buildRequest(byte[] recipient, byte[] rootId, byte[] ephemeralPublicKey) {
    RequestV1.Request request = RequestV1.Request.newBuilder()
            .setVersion(VERSION)
            .setType("dark-crystal/request")
            .setTimestamp(System.currentTimeMillis())
            .setRecipient(ByteString.copyFrom(recipient))
            .setRoot(ByteString.copyFrom(rootId))
            .setEphemeralPublicKey(ByteString.copyFrom(ephemeralPublicKey)) // TODO this should be optional
            .build();
    return request.toByteArray();
  }
  public byte[] buildReply(byte[] recipient, byte[] rootId, byte[] shardData, byte[] branch) {
    return ReplyV1.Reply.newBuilder()
            .setVersion(VERSION)
            .setType("dark-crystal/reply")
            .setTimestamp(System.currentTimeMillis())
            .setRecipient(ByteString.copyFrom(recipient))
            .setRoot(ByteString.copyFrom(rootId))
            .setShard(ByteString.copyFrom(shardData))
            .setBranch(ByteString.copyFrom(branch))
            .build()
            .toByteArray();
  }
  public byte[] buildForward(byte[] recipient, byte[] rootId, byte[] shardData, byte[] branch) {
    return ForwardV1.Forward.newBuilder()
            .setVersion(VERSION)
            .setType("dark-crystal/forward")
            .setTimestamp(System.currentTimeMillis())
            .setRecipient(ByteString.copyFrom(recipient))
            .setRoot(ByteString.copyFrom(rootId))
            .setShard(ByteString.copyFrom(shardData))
            .setBranch(ByteString.copyFrom(branch))
            .build()
            .toByteArray();
  }
}
